<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$catalogs = new Catalogos(new Config());
$loader->get('src/Model/Mail');

$mensagem_erro = false;

if (Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'send'){

    $link = URL_BASE.'/bo/assets/upload/'.$catalogs->getCatalogoLink($_POST['dados'][1]['value'])['catalogo'];

    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $html = '<h1>Catálogo para download da Eurotecnologia</h1>';
    $html .= '<a target="_blank" href="'.$link.'">Clique aqui para fazer o download.</a>';

    if (SendMail::trySendMail('Catálogo Eurotenologia', $_POST['dados'][0]['value'], $html, new PHPMailer(true))) {
      echo json_encode(array('1', '<strong>Sucesso!</strong> Catalogo enviado com sucesso.'));
      exit();
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> O sistema não conseguiu enviar o e-mail.'));
      exit();
    }
    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  }

}

if($action == 'index'){

  $catalogos = $catalogs->getCatalogos();
  $module_data['catalogos'] = $catalogos;

}

  // echo '<pre>';
  // print_r(array_column($catalogos, 'nome'));
  // echo '</pre>';

?>

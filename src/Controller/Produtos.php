<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$prod = new Produtos(new Config());
$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());
$loader->get('src/Model/Marcas');
$marcas = new Marcas(new Config());
$loader->get('src/Model/Mail');

$mensagem_erro = false;

if (Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'ativar'){
    $prod->ativaProd($_POST['id']);
  } else if($action == 'inativar'){
    $prod->inativaProd($_POST['id']);
  }

  if($action == 'send'){

    $produto = $prod->getProdutoMarca(array_pop($_POST['dados'])['value']);

    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $html = '<h1>Pedido de orçamento do produto '.$produto['referencia'].'</h1>';
    $html .= '<p>O cliente '.$_POST['dados'][0]['value'].' pediu um orçamento do produto '.$produto['nome'].' no site da Eurotenologia</p>';
    $html .= '<p>Foi enviado o email de contacto '.$_POST['dados'][1]['value'].' com a seguinte mensagem:</p>';
    $html .= '<p>'.$_POST['dados'][2]['value'].'</p>';
    // $html .= '<p>Qualquer dúvida entre em contacto.</p>';

    if (SendMail::trySendMail('Pedido de orçamento Eurotenologia', 'info@eurotecnologia.pt', $html, new PHPMailer(true))) {
      echo json_encode(array('1', '<strong>Sucesso!</strong> Pedido de orçamento enviado com sucesso.'));
      exit();
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> O sistema não conseguiu enviar o e-mail.'));
      exit();
    }
    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  }

}

if($action == 'index'){

  $produtos = $prod->readProductsAndAllFamilies();
  $module_data['produtos'] = $produtos;
  $module_data['actual_category'] = '';

} else if($action == 'detalhe'){

  $produto = $prod->getProdutoMarca($param);
  $module_data['produto'] = $produto;

  // echo '<pre>';
  // print_r($_POST);
  // echo '</pre>';

} else {

  // if (empty($param)) {
  //   $categoria = $cat->getCategoryBySlug($action);
  //   $produtos = $prod->getProductsByCategory($categoria['id']); //se for so action e so categoria
  // } else {
  //   $produtos = $prod->getProductsByMarcaAndCategory($action, $param); //se for action e param acction e marca e para categoria
  // }
  //
  // $module_data['produtos'] = $produtos;
  // $module_data['actual_category'] = $categoria['nome'];
}

  // echo '<pre>';
  // print_r($config->getMegaMenu($_SESSION['lg']));
  // echo '</pre>';

?>

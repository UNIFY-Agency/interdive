<?php

class News{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //status 0 proximos
    //status 1 reaizados

    public function getNews(){
      $select = $this->mysql->prepare('SELECT * FROM news WHERE 1 ORDER BY id DESC;');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertNews($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO news (title, status, conteudo, criado) VALUES (:title, :status, :conteudo, :criado);');
            $cadastra->bindValue(':title', $dados['title'], PDO::PARAM_STR);
            $cadastra->bindValue(':status', $dados['status'], PDO::PARAM_STR);
            $cadastra->bindValue(':conteudo', $dados['conteudo'], PDO::PARAM_STR);
            $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readNews($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM news WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM news WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM news WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editNews($dados){
      $update = $this->mysql->prepare('UPDATE news SET title = :title, status = :status, conteudo = :conteudo, atualizado = :atualizado WHERE id = :id ');
      $update->bindValue(':title', $dados['title'], PDO::PARAM_STR);
      $update->bindValue(':status', $dados['status'], PDO::PARAM_STR);
      $update->bindValue(':conteudo', $dados['conteudo'], PDO::PARAM_STR);
      $update->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
      $update->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM news WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}

jQuery(document).ready(function() {
  jQuery('.cookie-ok').click(function() {
    //console.log(jQuery(this).attr('class'));
    var url = URL_BASE + '/cookies/allow/';
    jQuery.post(url, function() {
      jQuery('.notice-bottom-bar').hide();
    });
  });
});

$('.open-menu-mobile').click(function() {
  $('.menu-mobile').show('slide');
});
$('.menu-mobile-close').click(function() {
  $('.menu-mobile').hide('slide');
});

$('.dropdown-item-mobile').click(function() {
  $('.menu-mobile').hide('slide');
});

$('.modal-button').click(function(){
  $('.product-title-modal').html($(this).find('.product-title').html());
  $('.product-subtitle-modal').html($(this).find('.product-subtitle').html());
  $('.product-desc-modal').html($(this).find('.product-desc').html());
  $('.product-detail-modal').html($(this).find('.product-detail').html());
  $('.product-image-modal').attr('src', $(this).find('.product-image').attr('src'));
});

$(document).on('click', '.close', function() {
  $('.product-title-modal').empty();
  $('.product-subtitle-modal').empty();
  $('.product-desc-modal').empty();
  $('.product-detail-modal').empty();
  $('.product-image-modal').attr('src', '');
});

$(document).on('click', '.btn-download', function() {
  $('#defaultModal').fadeOut();
});
